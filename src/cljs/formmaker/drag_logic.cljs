(ns formmaker.drag-logic
  (:require
   [goog.events :as events])
  (:import [goog.events EventType]))

(defn inside-rect?
  [point rect]
  "For a `rect`angle defined by a given element, check if the current mouse `point` falls within."
  ;; TODO Not sure how much this will break for a whole lot of elements,
  ;; especially with fast dragging. I think it would be best to eventually
  ;; restrict it to its neighborhood.

  (and
   (> (:left rect) (:x point) (:right rect))
   (> (:bottom rect) (:y point) (:top rect))))

(defn neighborhood []
  "Constantly recalculating whether I'm inside every given box is slow and
  unnecessary. Define a current neighborhood around the mouse position, and only
  consider those things that touch that neighborhood"
  {:x 100 :y 100})

(defn mouse-move-handler [offset element]
  (fn [evt]
    (let [x (- (.clientX evt) (:x offset))
          y (- (.clientY evt) (:y offset))]
      "Test if close to invalid positions here."
      (reset! element {:alive? true
                       :x      x
                       :y      y}))))

(defn mouse-up-handler [on-move]
  (fn me [evt]
    (events/unlisten js/window EventType.MOUSEMOVE
                     on-move)))

#_(defn mouse-down-handler [e]
  (let [{:keys [left top]}
        offset {:x (- (.clientX e) left)
                :y (- (.clientY e) top)}
        on-move (mouse-move-handler offset)]
    (events/listen js/window EventType.MOUSEMOVE on-move)
    (events/listen js/window EventType.MOUSEUP
                   (mouse-up-handler on-move))))

