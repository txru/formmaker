(ns formmaker.interop
    (:require
     [cljsjs.react-draggable]
     [reagent.core :as r]))

;; (def draggable (r/adapt-react-class "ReactDraggable"))
(def draggable-core (r/adapt-react-class (aget js/ReactDraggable "DraggableCore")))
(def react-draggable (r/adapt-react-class js/ReactDraggable))
(def default-draggable-props (aget js/ReactDraggable "defaultProps"))
(def create-draggable-data (aget js/ReactDraggable "createDraggableData"))

(defn draggable
  [element & {:keys [snap-to-value confine-to-body?]
              :or {snap-to-value 35 confine-to-body? true}}]
  (r/with-let [attrs {:grid [35 35]}]
    [react-draggable attrs [:div element]]))

(comment
  (defn on-drag-start
    [event, core-data]
    (print "Draggable: on-drag-start: " core-data)
    (let [this (r/current-component)
          original-on-start (:on-start (r/props (r/current-component)))]
      (if (original-on-start event (create-draggable-data this core-data))
        (do
          (reset! dragging true)
          (reset! dragged true))
        false)))

  (defn on-drag
    [event core-data]
    (print "Draggable: on-drag " core-data)
    (let [this (r/current-component)
          ui-data (create-draggable-data this core-data)
          bounds (:bounds (r/props this))
          new-state {:x (:x ui-data)
                     :y (:y ui-data)}]
      (if bounds
        (let [{x :x y :y} new-state]))))

  (defn on-drag-stop
    [])

  (defn draggable-recreate
    [position & [opt]]
    (let [{:keys
           name
           position
           on-drag
           x y
           slack-x 0 slack-y 0
           is-element-svg false}
          (merge draggable-defaults opt)
          dragging false
          dragged false]
      (r/create-class
       {:display-name name
        :component-will-mount
        (fn [this] (when (not
                          (and (.hasOwnProperty )))))
        :component-did-mount
        (fn [this]
          (when (and
                 (not= (type (.-SVGElement js/window)) js/undefined)
                 (= (type (.findDOMNode js/ReactDOM this)) (.-SVGElement js/window)))
            (reset! is-element-svg true)))
        :component-will-receive-props
        (fn [next-props]
          (when (and
                 (.hasOwnProperty next-props "position")
                 (or
                  (not= (:position/x x))
                  (not= (:position/y y))))
            (do (reset! x (:position/x next-props))
                (reset! y (:position/y next-props)))))
        :component-will-unmount
        (fn [] (reset! dragging false))}))))
