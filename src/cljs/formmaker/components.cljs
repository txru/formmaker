(ns formmaker.components
  (:require
   [clojure.string :as str]
   [cljs-pikaday.reagent :as pikaday]
   [reagent.core :as r]
   [formmaker.state :as state]
   [formmaker.instructions :as instructions]
   [formmaker.interop :as jso]))

(defn input []
  [:div
   {:class "column"
    :tooltip instructions/input}
   [:input {:placeholder "plain input"}]])


(defn typeahead-update [value ratom]
  ;; replace spaces with dot globs, and ram them at the beginning and end, if
  ;; there is anything in the input.
  (let [space-glob (re-pattern (if-not (str/blank? value)
                                 (str "(?i).*" (str/replace value #"\s+" ".*") ".*")
                                 ""))
        matches (filter #(re-matches space-glob %) state/states)]
    (reset! ratom matches)))


(defn typeahead []
  "Typeahead given a known, and hopefully short list. These elements can be
  provided either from paste, click and drag, or made dynamic with back-end
  database."
  (r/with-let [text-value (r/atom "")
               suggestions (r/atom [])]
    [:div
     {:class "column"
      :tooltip instructions/typeahead}
      [:input {:placeholder "autocomplete"
               :on-input #(typeahead-update
                           (-> % .-target .-value)
                           suggestions)}]
      (if @suggestions
        [:div.flex-container-column {:on-paste (fn [e] (print "Pasted"))}
         (map-indexed (fn [i sug]
                        [:div.suggestion {:key i} sug])
                      @suggestions)])]))

(defn textarea []
  "Vanilla textarea. The size is saved and presented to the end user."
  [:div
   {:class "column"
    :tooltip instructions/text-area}
    [:textarea {:placeholder "write long paragraphs here"}]])

(defn dropdown []
  "Dropdown menu with default options. When separated elements are either
  dragged on top (say from Excel) or pasted in, these become the new options."
  [:div
   {:class "column"
    :tooltip instructions/dropdown}
    [:select
     [:option "note: drag comma separated lists here, or paste from excel."]
     [:option "test2"]]])

(defn date-picker []
  "Simple date picker."
  (r/with-let [today (atom (js/Date))]
    [:div
     {:class "column"
      :tooltip instructions/date-picker}
      [pikaday/date-selector {:date-atom today}]]))

(defn toggler []
  "Just a bigger checkbox that togglers between green and red
  when clicked, and provides them to requests."
  (r/with-let [toggle-state (r/atom true)]
    [:div
     {:class "column flex-container"
      :tooltip instructions/toggler}
     [:div {:class (str "toggle-flex-box" " "
                        (if @toggle-state "alert-danger" "alert-success"))
            :on-click #(swap! toggle-state not)}
      [:div  [:h5 "toggle"]]]]))

(defn double-clicker [counter-atom flag-atom]
  (r/with-let [timeout (js/setTimeout #(reset! counter-atom 0))]
    (swap! counter-atom inc)
    (if (>= @counter-atom 2)
      (swap! flag-atom not)
      (js/clearTimeout timeout))))

(defn editable-text []
  "Wrap given text ELEMENT in `div` and turn the element into an `input` on
  double click, save on focus, unless no changes were made."
  (r/with-let [editing (r/atom false)
                     editing-text (r/atom "Header")
                     ref (r/atom nil)
                     text-element (r/atom :h5)]
    [:div
     {:class           "editable-text"
      :tooltip         instructions/editable-text
      :on-double-click #(reset! editing true)
      :on-blur         #(reset! editing false)}
     (if @editing
       ;; TODO Make the input look the same as the final element
       [:input
        {:class "text-element-editor"
         :default-value @editing-text
         ;; TODO Handle when the text here is nil
         :on-input #(reset! editing-text (-> % .-target .-value))}]
       [@text-element @editing-text])]))
