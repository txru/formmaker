(ns formmaker.app
    (:require
     [cljsjs.react-draggable]
     [reagent.core :as r :refer [atom]]
     [formmaker.views :as v]
     ))

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [v/draggable-source] (.getElementById js/document "container")))

(defn init []
  (mount-root))
