(ns formmaker.instructions)

(def editable-text"Double-click to edit this text, or click once to size down incrementally.")
(def typeahead "Type to match values in array, and then select. Paste or click
and drag either comma-separated values or column from Excel for new values.")
(def input "Simple typing. Right click to resize.")
(def text-area "Larger input box. Click and drag or right click to resize.")
(def date-picker "Just a date picker.")
(def toggler "True, false or nil value for easy clicking. Right click for color adjustment.")
(def dropdown "Paste or click and drag comma-separated values or column from Excel to populate.")
