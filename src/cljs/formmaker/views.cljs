(ns formmaker.views
  (:require
   [reagent.core :as r]
   [formmaker.components :as component]
   [formmaker.instructions :as instructions]
   [formmaker.interop :as jso]))

;; -------------------------
;; Views

(defn collect-positions []
  "TODO After each drag motion, this should record the position of the thing
   that has changed. This will be a map containing item, attributes, and
   position.")

(defn foo [props]
  (r/create-class
   {:component-did-mount
    (fn [this]
      (.log js/console (r/props this))
      (.log js/console (r/children this))
      (.log js/console (.-propName props)))
    :reagent-render
    (fn []
      [:h1 (str "Hello " (:name props))])}))

(defn draggable-source []
  "Necessary elements are dragged from here, and recreated when used.

  Elements: Normal input, textarea input, typeahead input (this will require
  coordination with server, more complicated), radio buttons, checkboxes,
  dropdown list (paste from Excel option), date picker, headings"
  [:div {:class "row show-grid"}
   [jso/draggable [component/editable-text]]
   [jso/draggable [component/typeahead]]
   [jso/draggable [component/input]]
   [jso/draggable [component/textarea]]
   [jso/draggable [component/date-picker]]
   [jso/draggable [component/toggler]]
   [jso/draggable [component/dropdown]]])
