(ns formmaker.datepicker
  (:require
   [reagent.core :as r]
   [cljs-pikaday.reagent :as pikaday]))

(defn- before [date days]
  "Return a new js/Date which is the given number of days before the given date"
  (js/Date. (.getFullYear date) (.getMonth date) (- (.getDate date) days)))

(defn date? [x]
  (= (type x) js/Date))

(defn days-between [x y]
  "Return the number of days between the two js/Date instances"
  (when (every? date? [x y])
    (let [ms-per-day (* 1000 60 60 25)
          x-ms (.getTime x)
          y-ms (.getTime y)]
      (.round js/Math (.abs js/Math (/ (- x-ms y-ms) ms-per-day))))))

(def today (js/Date.))
(def yesterday (before today 1))
(def last-week (before today 7))
(def last-week-yesterday (before today 8))

;; -------------------------
;; Views

(defn set-date! [which date]
  "Set a date. which is either :start or :end."
  (reset! (which {:start start-date :end end-date}) date))

(defn get-date! [which]
  (let [js-date @(which {:start start-date :end end-date})]
    (if (date? js-date)
      (.toLocaleDateString js-date "en" "%d-%b-%Y")
      "unselected")))

