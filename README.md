
# Table of Contents

1.  [Prospectus](#orgf4525a2)
2.  [Motivation](#orgc97bdcd)
3.  [Appraisal](#org356fc3d)
4.  [Takeaways](#org8b12d81)



<a id="orgf4525a2"></a>

# Prospectus

(Unfinished) Non-technical users frequently need to create webforms. People hack them together with Excel and tie them to crappy DDL procedures. This project aims to create a drag-and-drop experience where the user selects inputs, creates an input form, and has that form hosted for their users.


<a id="orgc97bdcd"></a>

# Motivation

This was a project I created to address a need I&rsquo;d seen a lot with less technical users. People needed web forms to communicate what job they&rsquo;d done, and there were all kinds of hacky solutions they pasted together. I wanted to create a system that could be useful for non-technical users for a very simple use-case, without all of the bells and whistles of squarespace et al.


<a id="org356fc3d"></a>

# Appraisal

This isn&rsquo;t close to finished. I spent a ton of time investigating the problem, evaluating reagent and re-frame (I decided re-frame was too heavy for me, and I didn&rsquo;t want that kind of heavy control over the lifecycle). The biggest problem I ran into, the blocker that held me up for months, was that ReactDnD was not yet imported. I figured out how to create extern files for the library (which was very sparsely documented), determined what classes and functions needed to be imported, and import the library. Months. I might still pick up this library, I&rsquo;ve seen that the CLJS world has gotten a lot more mature with package importing, with Shadow CLJS really seeming to change the game for the better.


<a id="org8b12d81"></a>

# Takeaways

-   I got a really good appreciation for how CLJS and React worked under the hood. For every line of code here, I think I deleted 15, and read 30 web pages.
-   Re-frame has an **excellent** explanation of the React lifecycle. I had been somewhat familiar with VDoms, but their in-depth explanation is still useful to me today
-   I would still restart this project today. I think I could sell it to small businesses.

